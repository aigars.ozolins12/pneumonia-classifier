import cv2 as cv
from os import walk
import os
import random

class DataProcesser():
    def __init__(self, image_dimensions):
        self.image_dir_path = "\\images"
        self.image_dimensions = image_dimensions

    def __prepare_image(self, img):
        grayscale_image = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        resized_grayscale = cv.resize(grayscale_image, self.image_dimensions)
        return resized_grayscale

    def __make_treshold(self, img):
        ret, treshold_image = cv.threshold(img, 185, 255, cv.THRESH_BINARY)
        return treshold_image

    def __rotate(self,img):
        rand_int = random.randint(0,2)
        if rand_int == 0:
            img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
        elif rand_int == 1:
            img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
        else:
            img = cv.rotate(img, cv.ROTATE_180)
        return img

    def __flip(self, img):
        rand_int = random.randint(0,1)
        if rand_int == 0:
            img = cv.flip(img, 1)
        else:
            img = cv.flip(img, -1)
        return img

    def enrich_dataset(self, images, labels):
        final_images = []
        final_labels = []
        for image,label in zip(images,labels):
            final_images.append(image)
            final_labels.append(label)

            final_images.append(self.__make_treshold(image))
            final_labels.append(label)

            final_images.append(self.__rotate(image))
            final_labels.append(label)

            final_images.append(self.__flip(image))
            final_labels.append(label)

        return final_images, final_labels

    def __open_image(self, image_path):
        return cv.imread(image_path)

    def __get_ready_image(self, image_path):
        img = self.__open_image(image_path)
        img = self.__prepare_image(img)
        return img

    def __get_all_normal_images(self):
        return self.__get_all_images_from_dir("NORMAL")

    def __get_all_pneumonia_images(self):
        return self.__get_all_images_from_dir("PNEUMONIA")

    def get_labeled_images(self):
        normal_images = self.__get_all_normal_images()
        pneumonia_images = self.__get_all_pneumonia_images()
        pneumonia_images = pneumonia_images[:len(normal_images)]
        normal_labels = [0 for x in range(len(normal_images))]
        pneumonia_labels = [1 for x in range(len(pneumonia_images))]

        return {"normal_images":normal_images,
                "pneumonia_images":pneumonia_images,
                "normal_labels":normal_labels,
                "pneumonia_labels":pneumonia_labels}

    def __get_all_images_from_dir(self, directory_name):
        images = []
        max_count = 1000
        pneumonia_image_count = 0
        normal_image_count = 0
        path = os.getcwd() + self.image_dir_path + "\\" + directory_name
        image_names = os.listdir(path)
        for image_name in image_names:
            full_image_path = path + "\\" + image_name
            images.append(self.__get_ready_image(full_image_path))
            normal_image_count += 1
        return images
