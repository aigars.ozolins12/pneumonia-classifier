import cv2 as cv
import numpy as np


blank = np.zeros((500,500,3), dtype='uint8')
cv.imshow('Blank',blank)
# healthy_lung_image = cv.imread('images/NORMAL/IM-0001-0001.jpeg')
#
#
# cv.imshow('X-RAY of healthy lung',healthy_lung_image)

#Draw a square
cv.rectangle(blank,(0,0),(blank.shape[1]//2,blank.shape[0]//2),(0,255,0),thickness=-1)
cv.imshow('Rectangle',blank)


#Draw a circle
cv.circle(blank, (250,250),40,(0,0,255),thickness=3)
cv.imshow("Circle",blank)


#Write text
cv.putText(blank,"Image of healthy lung",(255,255),cv.FONT_HERSHEY_TRIPLEX,1.0,(0,255,0),thickness=2)
cv.imshow("Text",blank)
cv.waitKey(0)
