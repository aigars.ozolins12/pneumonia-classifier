import cv2 as cv

healthy_lung_image = cv.imread('images/NORMAL/IM-0001-0001.jpeg')

cv.imshow('X-RAY of healthy lung',healthy_lung_image)

#Converting image to greyscale
gray = cv.cvtColor(healthy_lung_image, cv.COLOR_BGR2GRAY)
cv.imshow('Grayscale',gray)


#Blur image
blur =  cv.GaussianBlur(healthy_lung_image, (3,3),cv.BORDER_DEFAULT)
cv.imshow('Blur',blur)


#Edge cascade (How to find edges)
canny = cv.Canny(healthy_lung_image, 125, 175)
cv.imshow('Čanny',canny)


#Image dilation
dilated = cv.dilate(canny, (3,3), iterations =1)
cv.imshow("Dilated",dilated)


#Resize
resized = cv.resize(healthy_lung_image, (500,500)) #Ignores aspect ratio

#Cropping
cropped = healthy_lung_image[50:200, 200:400]


cv.waitKey(0)
