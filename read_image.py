import cv2 as cv

def rescale_frame(frame, scale = 0.75):
    height = int(frame.shape[0] * scale)
    width = int(frame.shape[1] * scale)
    dimensions = (width,height)
    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


healthy_lung_image = cv.imread('images/NORMAL/IM-0001-0001.jpeg')
healthy_lung_image = rescale_frame(healthy_lung_image,0.5)

cv.imshow('X-RAY of healthy lung',healthy_lung_image)

cv.waitKey(0)





