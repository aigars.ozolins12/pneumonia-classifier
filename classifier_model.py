import tensorflow as tf
from DataProcesser import DataProcesser
import numpy as np
import random

def create_confusion_matrix(predicted_labels, actual_labels):
    confusion_matrix = {"TP":0,"TN":0,"FP":0,"FN":0}
    for predicted_label, actual_label in zip(predicted_labels, actual_labels):
        if predicted_label == actual_label:
            if predicted_label == 1:
                confusion_matrix["TP"] += 1
            if predicted_label == 0:
                confusion_matrix["TN"] += 1
        else:
            if predicted_label == 1 and actual_label == 0:
                confusion_matrix["FP"] += 1
            if predicted_label == 0 and actual_label == 1:
                confusion_matrix["FN"] += 1
    return confusion_matrix

image_dimensions = (600,600)
dp = DataProcesser(image_dimensions)

result_dict = dp.get_labeled_images()
result_set = result_dict["normal_images"] + result_dict["pneumonia_images"], result_dict["normal_labels"] + result_dict["pneumonia_labels"]

dict_list = []

for image,label in zip(result_set[0],result_set[1]):
    new_dict = {"image":image,"label":label}
    dict_list.append(new_dict)


result_set = random.shuffle(dict_list)
images = []
labels = []
for dictionary in dict_list:
    images.append(dictionary["image"])
    labels.append(dictionary["label"])

training_labels = labels[:int(len(labels)*0.7)]
test_labels = labels[int(len(labels)*0.7):]

training_images = images[:int(len(images)*0.7)]
training_images, training_labels = dp.enrich_dataset(training_images, training_labels)
test_images = images[int(len(images)*0.7):]

training_images = np.array(training_images) // 255.0
test_images = np.array(test_images) // 255.0


print("Shape of trainign imagaes => " + str(training_images.shape))

training_images = training_images.reshape(training_images.shape[0],600,600,1)
test_images = test_images.reshape(test_images.shape[0],600,600,1)



model = tf.keras.models.Sequential([
                                    tf.keras.layers.Conv2D(32,(9, 9),activation='relu',input_shape=(600,600,1)),#32, (9,9)
                                    tf.keras.layers.MaxPooling2D((2,2)),
                                    tf.keras.layers.Conv2D(32,(9,9),activation='relu'),#32 (9,9)
                                    tf.keras.layers.MaxPooling2D((2, 2)),
                                    tf.keras.layers.Conv2D(32, (9, 9), activation='relu'),#32 (9,9)
                                    tf.keras.layers.Flatten(),
                                    tf.keras.layers.Dense(166, activation='relu'),#133/ best result #166
                                    #tf.keras.layers.Dropout(0.5),#0.5
                                    tf.keras.layers.Dense(2)])
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

dataset = tf.data.Dataset.from_tensor_slices((training_images, training_labels))

batches = dataset.shuffle(len(labels)).batch(32)
model.fit(batches, epochs=3)#4 epochs #FN % 0.06696428571428571

predicted_classes = model.predict(test_images)
predicted_labels = [np.argmax(x)for x in predicted_classes]

prediction_result_matrix = create_confusion_matrix(predicted_labels, test_labels)
factual_matrix = create_confusion_matrix(test_labels, test_labels)

print(prediction_result_matrix)
print(factual_matrix)
print(f"FN % {prediction_result_matrix['FN']/factual_matrix['TP']}")


#Dropout (0.5)
#{'TP': 415, 'TN': 438, 'FP': 40, 'FN': 52}
#{'TP': 467, 'TN': 478, 'FP': 0, 'FN': 0}
#FN % 0.11134903640256959

#Dropout (0.2)
#{'TP': 436, 'TN': 436, 'FP': 33, 'FN': 40}
#{'TP': 476, 'TN': 469, 'FP': 0, 'FN': 0}
# 0.08403 % FN/TP

# Dropout (0.1)
#{'TP': 431, 'TN': 444, 'FP': 40, 'FN': 30}
#{'TP': 461, 'TN': 484, 'FP': 0, 'FN': 0}
#0.06507 % FN/TP

#No dropout
#{'TP': 461, 'TN': 396, 'FP': 57, 'FN': 31}
#{'TP': 492, 'TN': 453, 'FP': 0, 'FN': 0}
#FN % 0.06300813008130081

#No Droupout, 111 dense neurons
#{'TP': 433, 'TN': 408, 'FP': 53, 'FN': 51}
#{'TP': 484, 'TN': 461, 'FP': 0, 'FN': 0}
#FN % 0.10537190082644628

#No dropout, 133 dense neurons (16 ; (3,3)
#{'TP': 440, 'TN': 431, 'FP': 33, 'FN': 41}
#{'TP': 481, 'TN': 464, 'FP': 0, 'FN': 0}
#FN % 0.08523908523908524

#No dropout 133 dense neurons (32; (12,12)
#FN % 11

#No Dropout 144 dense neurons (32,9,9)
#FN % 10

#No dropout 144 dense neurons (16,(3,3))
#{'TP': 429, 'TN': 432, 'FP': 49, 'FN': 35}
#{'TP': 464, 'TN': 481, 'FP': 0, 'FN': 0}
#FN % 0.07543103448275862


#No dropout 144,32 dense neurons (16,(3,3))
#{'TP': 431, 'TN': 424, 'FP': 46, 'FN': 44}
#{'TP': 475, 'TN': 470, 'FP': 0, 'FN': 0}
#FN % 0.09263157894736843

#No dropout 144,64 dense neurons (16,(3,3))
#{'TP': 437, 'TN': 403, 'FP': 76, 'FN': 29}
#{'TP': 466, 'TN': 479, 'FP': 0, 'FN': 0}
#FN % 0.06223175965665236

#No dropout 144,84 dense neurons (16,(3,3))
#{'TP': 452, 'TN': 412, 'FP': 42, 'FN': 39}
#{'TP': 491, 'TN': 454, 'FP': 0, 'FN': 0}
#FN % 0.07942973523421588

#No dropout 160 dense neuros (16,(3,3))
#{'TP': 440, 'TN': 426, 'FP': 48, 'FN': 31}
#{'TP': 471, 'TN': 474, 'FP': 0, 'FN': 0}
#FN % 0.06581740976645435

#No dropout 111 dense neurons (16,(3,3))
#{'TP': 445, 'TN': 395, 'FP': 73, 'FN': 32}
#{'TP': 477, 'TN': 468, 'FP': 0, 'FN': 0}
#FN % 0.06708595387840671


# model = tf.keras.models.Sequential([
#                                     tf.keras.layers.Conv2D(32,(9, 9),activation='relu',input_shape=(600,600,1)),#32, (9,9)
#                                     tf.keras.layers.MaxPooling2D((2,2)),
#                                     tf.keras.layers.Conv2D(32,(9,9),activation='relu'),#32 (9,9)
#                                     tf.keras.layers.MaxPooling2D((2, 2)),
#                                     tf.keras.layers.Conv2D(32, (9, 9), activation='relu'),#32 (9,9)
#                                     tf.keras.layers.Flatten(),
#                                     tf.keras.layers.Dense(166, activation='relu'),#133/ best result #166
#                                     #tf.keras.layers.Dropout(0.5),#0.5
#                                     tf.keras.layers.Dense(2)])
# 69/69 [==============================] - 2879s 41s/step - loss: 0.0635 - accuracy: 0.9792
# {'TP': 417, 'TN': 423, 'FP': 43, 'FN': 62}
# {'TP': 479, 'TN': 466, 'FP': 0, 'FN': 0}
# FN % 0.12943632567849686