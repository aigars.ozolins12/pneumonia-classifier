import cv2 as cv
import numpy as np


healthy_lung_image = cv.imread('images/NORMAL/IM-0001-0001.jpeg')

cv.imshow('X-RAY of healthy lung',healthy_lung_image)


#Translation
#-x ==> left
#-y ==> Up
#x ==> Right
#y ==> Down
def translate(img, x, y):
    translation_matrix = np.float32([1,0,x],[0,1,y])
    dimensions = (img.shape[1], img.shape[0])
    return cv.warpAffine(img, translation_matrix, dimensions)


#Rotation
def rotate(image, angle, rotation_point=None):
    (height, width) = image.shape[:2]
    if rotation_point is None:
        rotation_point = (width//2, height//2)

    rotation_matrix = cv.getRotationMatrix2D(rotation_point, angle, 1.0)
    dimensions = (width,height)

    return cv.warpAffine(image, rotation_matrix, dimensions)


#Resizing
resized = cv.resize(healthy_lung_image, (500,500), interpolation=cv.INTER_CUBIC)#INTER_CUBIC or INTER_LINEAR is used for resizing to bigger image
cv.imshow("Resized", resized)


#Flipping
flip = cv.flip(healthy_lung_image,0) #0=> flip vertically, 1=> flip horizontally, -1 vertically and horizontally

#Cropping
cropped = healthy_lung_image[200:400, 300:400]

cv.waitKey(0)
